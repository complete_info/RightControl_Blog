﻿using RightControl.IRepository;
using RightControl.Model;

namespace RightControl.Repository
{
    public class CommentRepository : BaseRepository<CommentModel>, ICommentRepository
    {
    }
}
