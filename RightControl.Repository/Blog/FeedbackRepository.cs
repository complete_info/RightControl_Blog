﻿using RightControl.IRepository;
using RightControl.Model;

namespace RightControl.Repository
{
    public class FeedbackRepository : BaseRepository<FeedbackModel>, IFeedbackRepository
    {
    }
}
