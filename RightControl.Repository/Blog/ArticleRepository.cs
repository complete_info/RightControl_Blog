﻿using RightControl.IRepository;
using RightControl.Model;

namespace RightControl.Repository
{
    public class ArticleRepository : BaseRepository<ArticleModel>, IArticleRepository
    {
    }
}
