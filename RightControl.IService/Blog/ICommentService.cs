﻿using RightControl.Model;

namespace RightControl.IService
{
    public interface ICommentService : IBaseService<CommentModel>
    {
    }
}
